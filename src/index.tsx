import React from 'react';
import ReactDOM from "react-dom";
import App from "./App";
import {
  DefaultTheme,
  ThemeProvider,
} from 'styled-components';

const theme: DefaultTheme = {
  mainColor: '#6DA9E9',
  borderColor: '#f2f2f2',
  textColor: '#666666',
};

ReactDOM.render((
  <ThemeProvider theme={theme}>
    <App />
  </ThemeProvider>
), document.getElementById('root'));
