import styled from 'styled-components';

export const TableContainer = styled.div`
  display: inline-flex;
  margin: 30px 0;
  max-height: 440px;
  border-bottom: 1px solid ${props => props.theme.borderColor};
  border-radius: 2px;
  overflow-y: auto;
`;

export const Table = styled.table`
  position: relative;
  width: 100%;
  border-collapse: separate;
  border-spacing: 0;
`;

export const TableCell = styled.td`
  font-size: 15px;
  color: #666666;
  padding: 20px 30px;
  border-bottom: 1px solid ${props => props.theme.borderColor};
  background-color: #ffffff;
`;

export const TableHead = styled.thead`
  ${TableCell} {
    position: sticky;
    top: 0;
  
    font-size: 18px;
    color: #ffffff;
    line-height: 1.2;
    background-color: ${props => props.theme.mainColor};
  }
`;

export const TableBody = styled.tbody`
  ${TableCell} {
    &:first-child {
      border-left: 1px solid ${props => props.theme.borderColor};
    }
    
    &:last-child {
      border-right: 1px solid ${props => props.theme.borderColor};      
    }
  }
`;

export const TableRow = styled.tr``;
