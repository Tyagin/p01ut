import styled from 'styled-components';

const Button = styled.button`
    border: none;
    font-family: inherit;
    font-size: inherit;
    color: inherit;
    background: none;
    border-radius: 2px;
    cursor: pointer;
    padding: 15px 30px;
    display: inline-block;
    text-transform: uppercase;
    letter-spacing: 1px;
    font-weight: 700;
    outline: none;
    position: relative;
`;

export default Button;
