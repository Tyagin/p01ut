import React from 'react';
import Button from './Button';
import styled, {
  css,
  DefaultTheme,
} from 'styled-components';
import {
  lighten,
} from 'polished';

const lightenBackground = (props: { theme: DefaultTheme, disabled?: boolean }) => props.disabled ? lighten(.3, 'gray') : lighten(.18, props.theme.mainColor);

const PushButton = styled(Button)<React.HTMLAttributes<HTMLButtonElement> & { disabled?: boolean }>`
   background: ${props => (props.disabled && 'gray') || props.theme.mainColor};
   color: #fff;
   box-shadow: 0 6px ${lightenBackground};
   
   ${props => !props.disabled && css`
      &:hover {
        box-shadow: 0 4px ${lightenBackground};
        top: 2px;
      }
   
      &:active {
        box-shadow: 0 0 ${lightenBackground};
        top: 6px;
      }
   `}
`;

export default PushButton;
