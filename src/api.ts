import axios from './axios';
import {
  Solve,
  SolveBody,
} from './types';

interface RequestOptions {
  file: File;
  url: string;
}

export const getTableFromCsv = async <T>(options: RequestOptions): Promise<T | null> => {
  let response = null;

  try {
    const formData = new FormData();
    formData.append('file', options.file);

    const axiosResponse = await axios.post<T>(options.url);
    response = axiosResponse.data;
  } catch (e) {
    console.error(e);
  }

  return response;
};

export const getSolve = async (body: SolveBody): Promise<{ data: Solve[] }> => {
  let response = null;

  try {
    const axiosResponse = await axios.post('/solve', body);
    response = axiosResponse.data;
  } catch (e) {
    console.error(e);
  }

  return response;
};

export const exportSolve = async (body: SolveBody): Promise<Blob> => {
  let response = null;

  try {
    const axiosResponse = await axios.post('/solve.csv', body, { responseType: 'blob' });
    response = axiosResponse.data;
  } catch (e) {
    console.error(e);
  }

  return response;
};
