import { hot } from 'react-hot-loader';
import React, {
  useCallback,
  useState,
} from 'react';
import FileButton from './FileButton';
import styled, {
  createGlobalStyle,
  css,
} from 'styled-components';
import {
  exportSolve,
  getSolve,
  getTableFromCsv,
} from './api';
import {
  Employee,
  Event,
  Grades,
  Solve,
} from './types';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from './Table';
import PushButton from './PushButton';
import { saveAs } from 'file-saver';

type FileEvent = React.ChangeEvent<HTMLInputElement>;

const GlobalStyle = createGlobalStyle`
  html {
    font-family: Roboto, sans-serif;
    font-size: 14px;
    font-weight: 400;
  }
  
  body {
    margin: 0;
    background-color: rgb(246, 246, 250);
  }
 
  * {
    box-sizing: border-box;
  }
`;

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  max-width: 2480px;
  margin: 0 auto;
  padding: 24px;
`;

const Paper = styled.div<{ disabled?: boolean }>`
  position: relative;
  display: inline-flex;
  flex-direction: column;
  width: 500px;
  margin: 10px;
  padding: 16px;
  border-radius: 4px;
  background-color: #fff;
  box-shadow: 0 2px 1px -1px rgba(0, 0, 0, 0.2),
    0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12);
  transition: all 300ms;
  
  &:hover {
    transform: translateY(-4px);
  }
  
  ${props => props.disabled && css`
      &:after {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-color: black;
        border-radius: 4px;
        opacity: .3;
        cursor: no-drop;
        z-index: 1;
      }
  `}
`;

const Text = styled.div`
  font-size: 20px;
  margin-top: 20px;
  color: ${props => props.theme.textColor};
`;

const TablePreview = ({ colsNum }: { colsNum: number }) => (
  <>
    {Array(6).fill(null).map((item, index) => (
      <TableRow key={index}>
        {Array(colsNum).fill(null).map((item1, index1) => (
          <TableCell key={index1}>
            <div style={{ height: 20 }}/>
          </TableCell>
        ))}
      </TableRow>
    ))}
  </>
);

const GradesTable = ({ grades, ratio }: { grades: Grades[]; ratio: string }) => {
  return (
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>{ratio}</TableCell>
            {grades[0] ? grades[0].values.map((item, index) => (
              <TableCell key={index}>
                {index + 1}
              </TableCell>
            )) : (
               <>
                 <TableCell/>
                 <TableCell/>
               </>
             )}
          </TableRow>
        </TableHead>
        <TableBody>
          {grades.length ? grades.map(item => (
            <TableRow key={item.i}>
              <TableCell>{item.i}</TableCell>
              {item.values.map((item1, index) => (
                <TableCell key={index}>{item1}</TableCell>
              ))}
            </TableRow>
          )) : <TablePreview colsNum={3}/>}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const App = () => {
  const [employees, setEmployees] = useState<Employee[]>([]);
  const [events, setEvents] = useState<Event[]>([]);
  const [gradesC, setGradesC] = useState<Grades[]>([]);
  const [gradesQ, setGradesQ] = useState<Grades[]>([]);
  const [solve, setSolve] = useState<Solve[]>([]);

  const getInputFile = useCallback((e: FileEvent): File | undefined => {
    const files = e.target.files;
    if (files) {
      return Array.from(files)[0];
    }
    return undefined;
  }, []);

  return (
    <Container>
      <GlobalStyle/>
      {/* EMPLOYEES */}
      <Paper>
        <div>
          <FileButton label='Загрузить Сотрудников' onChange={async (e: FileEvent) => {
            const file = getInputFile(e);
            if (file) {
              const response = await getTableFromCsv<{ data: Employee[] }>({
                file,
                url: '/employees',
              });
              if (response) {
                setEmployees(response.data);
              }
            }
          }}/>
        </div>
        <Text>Загружено:</Text>
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>I</TableCell>
                <TableCell style={{ width: '100%' }}>Email</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {employees.length ? employees.map(item => (
                <TableRow key={item.i}>
                  <TableCell>{item.i}</TableCell>
                  <TableCell>{item.email}</TableCell>
                </TableRow>
              )) : <TablePreview colsNum={2}/>}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
      {/* EVENTS */}
      <Paper disabled={!employees.length}>
        <div>
          <FileButton label='Загрузить Мероприятия' onChange={async (e: FileEvent) => {
            const file = getInputFile(e);
            if (file) {
              const response = await getTableFromCsv<{ data: Event[] }>({
                file,
                url: '/events',
              });
              if (response) {
                setEvents(response.data);
              }
            }
          }}/>
        </div>
        <Text>Загружено:</Text>
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>I</TableCell>
                <TableCell style={{ width: '100%' }}>Название</TableCell>
                <TableCell>Vi</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {events.length ? events.map(item => (
                <TableRow key={item.i}>
                  <TableCell>{item.i}</TableCell>
                  <TableCell>{item.name}</TableCell>
                  <TableCell>{item.Vi}</TableCell>
                </TableRow>
              )) : <TablePreview colsNum={3}/>}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
      {/* GRADES_C */}
      <Paper disabled={!events.length}>
        <div>
          <FileButton label='Загрузить C' onChange={async (e: FileEvent) => {
            const file = getInputFile(e);
            if (file) {
              const response = await getTableFromCsv<{ data: Grades[] }>({
                file,
                url: '/grades/c',
              });
              if (response) {
                setGradesC(response.data);
              }
            }
          }}/>
        </div>
        <Text>Загружено M*N:</Text>
        <GradesTable grades={gradesC} ratio='Ci/j'/>
      </Paper>
      {/* GRADES_Q */}
      <Paper disabled={!gradesC.length}>
        <div>
          <FileButton label='Загрузить Q' onChange={async (e: FileEvent) => {
            const file = getInputFile(e);
            if (file) {
              const response = await getTableFromCsv<{ data: Grades[] }>({
                file,
                url: '/grades/q',
              });
              if (response) {
                setGradesQ(response.data);
              }
            }
          }}/>
        </div>
        <Text>Загружено M*N:</Text>
        <GradesTable grades={gradesQ} ratio='Qi/j'/>
      </Paper>
      <div style={{
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
      }}>
        <Paper style={{
          flexDirection: 'row',
          width: 'auto',
        }}>
          <PushButton disabled={!gradesQ.length} onClick={async () => {
            const response = await getSolve({
              employees,
              events,
              c: gradesC,
              q: gradesQ
            });
            setSolve(response.data);
          }}>
            Решить
          </PushButton>
          <PushButton style={{ marginLeft: 40 }} onClick={() => {
            setEmployees([]);
            setEvents([]);
            setGradesC([]);
            setGradesQ([]);
            setSolve([]);
          }}>
            Очистить все
          </PushButton>
        </Paper>
      </div>
      {/* SOLVE */}
      <Paper disabled={!solve.length} style={{ width: '100%' }}>
        <GradesTable grades={solve} ratio='Xi/j' />
        <div style={{ textAlign: 'right', }}>
          <PushButton onClick={async () => {
            const file = await exportSolve({
              employees,
              events,
              c: gradesC,
              q: gradesQ
            });
            saveAs(file, 'solve.csv');
          }}>
            Выгрузить в .csv
          </PushButton>
        </div>
      </Paper>
    </Container>
  );
};

export default hot(module)(App);
