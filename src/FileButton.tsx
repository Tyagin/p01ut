import React from 'react';
import styled from 'styled-components';
import PushButton from './PushButton';

interface Props extends React.HTMLAttributes<HTMLInputElement> {
  label: string;
  disabled?: boolean;
}

const Label = styled.label`
  position: relative;
  display: inline-flex;
`;

const Input = styled.input`
  position: absolute;
  opacity: 0;
  width: 100%;
  height: 100%;
`;

const FileButton = ({ label, disabled, ...props }: Props) => {
  return (
    <Label>
      <Input {...props} disabled={disabled} type='file' value='' />
      <PushButton as='div' disabled={disabled}>
        {label}
      </PushButton>
    </Label>
  );
};

export default FileButton;
