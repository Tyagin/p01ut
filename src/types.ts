export interface Employee {
  email: string;
  i: number;
}

export interface Event {
  Vi: number;
  i: number;
  name: string;
}

export interface Grades {
  i: number;
  values: number[];
}

export type Solve = Grades;

export interface SolveBody {
  employees: Employee[];
  events: Event[];
  c: Grades[];
  q: Grades[];
}
