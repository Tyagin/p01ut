import 'styled-components';

declare module 'styled-components' {
  interface DefaultTheme {
    mainColor: string;
    borderColor: string;
    textColor: string;
  }
}
